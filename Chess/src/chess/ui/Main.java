package chess.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Main {

    private static Socket socket;
    private static PrintWriter printWriter;
    private static Integer port = 8080;
    private static String serverip = "localhost";
    private static BufferedReader bufferedReader;
    private static String inputLine;

    public static void main(String[] args) {
	// write your code here
        int i = 0;
        String text = "";
        Scanner scan = new Scanner(System.in);
        try
        {
            socket = new Socket(serverip, port);
            printWriter = new PrintWriter(socket.getOutputStream(),true);
            Thread client = new Thread(new Runnable() {
                @Override
                public void run() {
                    printmsg();
                }
            });
            client.start();
            text = scan.nextLine();
            while (text.equals("exit") != true) {
                printWriter.println(text);
                text = scan.nextLine();
            }
            System.exit(1);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }

    public static void printmsg()
    {
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // Get the client messages
            while ((inputLine = bufferedReader.readLine()) != null) {
                System.out.println("got: " + inputLine);
            }
        }catch (IOException e) {
            System.out.println(e);
        }
    }

}
